package br.com.planb.CompletableFuture.factory;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

public class NBPExchangeRate implements ExchangeRate {

	 private static final String NBP_API_CURRENCY_RATE_PATH = "http://api.nbp.pl/api/exchangerates/rates/a/%s?format=json";

	    private final String url;
	    private final String currency;

	    NBPExchangeRate(String currency) {
	        this.currency = currency;
	        this.url = String.format(NBP_API_CURRENCY_RATE_PATH, currency);
	    }

	    @Override
	    public BigDecimal retrieve() {
	        RestTemplate restTemplate = new RestTemplate();
	        NBPApiResponse result = restTemplate.getForObject(url, NBPApiResponse.class);
	        BigDecimal currentRate = result.getRates()[0].getMid();

	        return currentRate;
	    }

	    private static class NBPApiResponse {

	        private String table;
	        private String currency;
	        private String code;
	        private Rates[] rates;

			public Rates[] getRates() {
				return rates;
			}
			
	        	
	    }

	    private static class Rates {

	        private String no;
	        private BigDecimal mid;

	        @JsonDeserialize(using = LocalDateDeserializer.class)
	        private LocalDate effectiveDate;		
			public BigDecimal getMid() {
				return mid;
			}
	        	
	    }

}
