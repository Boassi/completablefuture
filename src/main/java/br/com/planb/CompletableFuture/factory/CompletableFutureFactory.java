package br.com.planb.CompletableFuture.factory;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Component;

@Component
public class CompletableFutureFactory {
	
	 public CompletableFuture<BigDecimal> nbpRateFor(String currency) {
	        return prepareFuture(new NBPExchangeRate(currency));
	    }
	 
//	    public CompletableFuture<BigDecimal> fixerRateFor(String currency) {
//	        return prepareFuture(new FixerExchangeRate(currency));
//	    }
	 
//	    public CompletableFuture<BigDecimal> yahooRateFor(String currency) {
//	        return prepareFuture(new YahooFinanceExchangeRate(currency));
//	    }
	 
	    private CompletableFuture<BigDecimal> prepareFuture(ExchangeRate exchangeRate) {
	        return CompletableFuture
	                .supplyAsync(() -> exchangeRate.retrieve())
	                .exceptionally(t -> BigDecimal.ZERO);
	    }
}
