package br.com.planb.CompletableFuture.factory;

import java.math.BigDecimal;

public interface ExchangeRate {
	BigDecimal retrieve();
}
