package br.com.planb.CompletableFuture.controller;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.planb.CompletableFuture.factory.CompletableFutureFactory;

@RestController
public class BestCurrencyController {
	
	@Autowired
    private CompletableFutureFactory completableFutureFactory;

    @RequestMapping(value = "/{currency}", method = RequestMethod.GET)
    public BigDecimal bestCurrencyRate(@PathVariable String currency) {

        CompletableFuture<BigDecimal> nbpRate = completableFutureFactory.nbpRateFor(currency);
//        CompletableFuture<BigDecimal> fixerRate = completableFutureFactory.fixerRateFor(currency);
//        CompletableFuture<BigDecimal> yahooRate = completableFutureFactory.yahooRateFor(currency);

        CompletableFuture<BigDecimal> best = 
                nbpRate
                .thenCombineAsync(nbpRate, (nbp, fixer) -> selectBest(nbp, fixer))
                .thenCombineAsync(nbpRate, (combined, yahoo) -> selectBest(combined, yahoo));

        try {
            return best.get(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal selectBest(BigDecimal first, BigDecimal second) {
        return first.max(second);
    }
}
